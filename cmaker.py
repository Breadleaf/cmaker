'''
Author :
Bradley Kenneth Hutchings

Date :
October 29 2021

License:
GNU GPL_V3: 
https://www.gnu.org/licenses/gpl-3.0.en.html

Description:
This application was written to make writting "CMakeLists.txt" files faster for
my CSCI261 class so that I don't have to use CLION software. I don't have a
grudge, I simply perfer vim to CLION. In hindsight I could have written a vim
auto-complete with the :r command (see below,) however the idea of making a
command to do it for me with some extra functions to help automate seemed more
fun to do. Also I have a econ assignment due in 6 hours and 40 min that I have
about half way done. I kinda got sidetracked lol :D
ex:
:r <path>cmake-template.txt

cmake-template.txt:

cmake_minimum_required(VERSION 3.20)
project(name)

set(CMAKE_CXX_STANDARD 14)

add_executable(name)
'''

#!/usr/bin/python3

import sys, getopt, os

def main(argv):

    # Default Values
    cmake_minimum = "3.20"
    project_name = "Demo"
    usingSFML = False

    # Check to see if arguments are present and if they are valid
    try:
        opts, args = getopt.getopt(argv, "hce")
    except getopt.GetoptError:
        print("Useage:")
        print("cmaker -h -> Display help screen")
        print("cmaker -c -> Make CMakeLists.txt file")
        print("cmaker -e -> Build and Execute \"CMakeLists.txt\"")
        print("cmaker    -> Build \"CMakeLists.txt\"")
        sys.exit(2)

    # If there are no arguments make a directory called "build", then if there
    # is a "CMakeLists.txt" file present build the project in the build folder.
    if len(sys.argv) == 1:
        if os.path.exists("build") == False:
            os.system("mkdir build")

        if os.path.exists("CMakeLists.txt"):
            os.system("cd build && cmake .. && make && make")
        else:
            print("You need to make a CMakeLists.txt first")
            sys.exit()
    
    # Begin looking at arguments and do related functions.
    else:
        for opt, arg in opts:

            # Display "Help" menu.
            if opt == "-h":
                print("Useage:")
                print("cmaker -h -> Display help screen")
                print("cmaker -c -> Make CMakeLists.txt file")
                print("cmaker -e -> Build and Execute \"CMakeLists.txt\"")
                print("cmaker    -> Build \"CMakeLists.txt\"")
                sys.exit()

            # Begin the making of a custom "CMakeLists.txt" file.
            elif opt == "-c":

                # Ask the user if they want a custom cmake version minimum.
                print("Minimum cmake version:")
                print("Defult: 3.20")
                while True:
                    userInput = str(
                            input("Do you want to use a diffrent version: y/n "))
                    userInput = userInput.strip().lower()
                    if (userInput != "y") and (userInput != "n"):
                        print("Please enter a valid option")
                    else:
                        break
                if userInput == "y":
                    cmake_minimum = str(input("Enter the minimum version: "))
                    cmake_minimum = cmake_minimum.strip()

                # Ask the user if they want a custom project name.
                print("\nProject Name:")
                print("Default: Demo")
                while True:
                    userInput = str(
                            input("Do you want to use a diffrent name: y/n "))
                    userInput = userInput.strip().lower()
                    if (userInput != "y") and (userInput != "n"):
                        print("Please enter a valid option")
                    else:
                        break
                if userInput == "y":
                    project_name = str(input("Enter the project name: "))
                    project_name = project_name.strip()

                # Ask the user if they would like to use sfml.
                print("\nUse SFML?")
                print("Default: No")
                while True:
                    userInput = str(
                            input("Would you like to use SFML: y/n "))
                    userInput = userInput.strip().lower()
                    if (userInput != "y") and (userInput != "n"):
                        print("Please enter a valid option")
                    else:
                        break
                if userInput == "y":
                    usingSFML = True

                # Format "CMakeLists.txt" file.
                if usingSFML == True:
                    textToOutput = "cmake_minimum_required(VERSION " +\
                                    cmake_minimum + ")\nproject(" +\
                                    project_name +\
                                    ")\n\nset(CMAKE_CXX_STANDARD 14)\
                                    \n\ninclude_directories\
(\"/usr/local/include\")\
                                    \nlink_directories(\"/usr/local/lib\")\
                                    \n\nadd_executable(" + project_name +\
                                    " main.cpp)\
                                    \n\ntarget_link_libraries\
(" + project_name + " sfml-graphics\
 sfml-window sfml-system)"
                else:
                    textToOutput = "cmake_minimum_required(VERSION " +\
                                    cmake_minimum + ")\nproject(" +\
                                    project_name +\
                                    ")\n\nset(CMAKE_CXX_STANDARD 14)\
                                    \n\nadd_executable(" + project_name +\
                                    " main.cpp)"

                # Clear existing "CMakeLists.txt" if one exists, make new file.
                if os.path.exists("CMakeLists.txt"):
                    os.system("rm CMakeLists.txt")
                os.system("touch CMakeLists.txt")

                # Print to file.
                outputFile = open("CMakeLists.txt", "a")
                outputFile.write(textToOutput)
                outputFile.close()

            # Builds CMakeLists.txt and executes the file in build/
            elif opt == "-e":
                if os.path.exists("build") == False:
                    os.system("mkdir build")

                if os.path.exists("CMakeLists.txt"):
                    os.system("cd build && cmake .. && make && make")
                else:
                    print("You need to make a CMakeLists.txt first")
                    
                lines = []

                with open("CMakeLists.txt") as readCMakeList:
                    lines = readCMakeList.readlines()

                count = 0
                for line in lines:
                    count += 1
                    if line.startswith("add_executable"):
                        executeBuild = "./build/" +\
                                str(line.replace("add_executable(", "", 1).split(None, 1)[0])
                        break

                os.system(executeBuild)


if __name__ == "__main__":
    main(sys.argv[1:])
